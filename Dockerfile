FROM node:alpine

RUN apk add --no-cache git curl jq git-lfs

RUN npm install --cache /tmp/empty-cache -g semantic-release @semantic-release/gitlab @semantic-release/git @semantic-release/changelog @semantic-release/exec
